var path = require('path');
var express = require('express');

var router = express.Router();
var homecontroller = require(path.join(__dirname, '../controllers/HomeController.js'));
var jobcontroller = require(path.join(__dirname, '../controllers/JobController.js'));


router.use(function(req, res, next){
    console.log('MiddleWare --> AnaRoute');
    next();
});
router.get('/', homecontroller.get_home);
router.get('/getAllUser', homecontroller.get_allUser);
router.get('/createUser', homecontroller.create_user);
router.get('/deleteAllUser', homecontroller.delete_allUser);
router.get('/getAllRank', homecontroller.get_allRank);
router.get('/addpointsecond', homecontroller.add_pointsecond);

router.get('/jobs/sortrank', jobcontroller.sort_Rank_All);
router.get('/jobs/dayRank', jobcontroller.day_ranked);
router.get('/jobs/weekRank', jobcontroller.week_ranked);
router.get('/jobs/getAllRank', jobcontroller.get_allRank);


module.exports = router;