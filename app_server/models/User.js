var mongoose = require('mongoose');
// User Schema
var UserSchema = mongoose.Schema({
	username: {
		type: String,
		createIndexes: true
	},
	password: {
		type: String
	},
	age: {
		type: Number
	},
	lastdayranked: {
		type: Number
	},
	weeklyranked: {
		type: Number
	},
	score: {
		type: Number,
	},
	lastOnline: {
		type: Date,
	},
	online:{
		type: Boolean,
	}
});
var User = module.exports = mongoose.model('User', UserSchema);
User.createIndexes({ username : 1, online : 1, lastOnline : -1, score : -1 });

module.exports.removeAllUser = function(newUser, callback){
	User.remove({}, callback);
}

module.exports.createUser = function(newUser, callback){
	newUser.save(callback)
}

module.exports.getAllUser = function(callback){
	User.find({}, callback);
}

module.exports.getUserByUsername = function(username, callback){
	var query = {username: username};
	User.findOne(query, callback);
}