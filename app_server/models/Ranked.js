var mongoose = require('mongoose');
// User Schema
var WeeklyRankedSchema = mongoose.Schema({
	username: {
		type: String
	},
	xid: {
		type: String
	},
	rank: {
		type: String
	}
});
var WeeklyRanked = module.exports = mongoose.model('WeeklyRanked', WeeklyRankedSchema);
WeeklyRanked.createIndexes({ username : 1 ,  rank : -1 });

module.exports.createUser = function(newUser, callback){
    newUser.save(callback);
}

module.exports.getAllUser = function(callback){
	WeeklyRanked.find({}, callback);
}

module.exports.getUserByUsername = function(username, callback){
	var query = {username: username};
	WeeklyRanked.findOne(query, callback);
}


