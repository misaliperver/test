const User = require('../models/User');
const Ranked = require('../models/Ranked');
const moment = require("moment");

module.exports.get_home = function(req, res){
    res.render('index');
}

module.exports.delete_allUser = function(req, res){
    User.deleteMany({}, function(){
        res.json({success: true, message:"Hepsi silindi."});
    });
}

module.exports.get_allUser = function(req, res){
    User.getAllUser(function(err, data){
        res.json({data: data});
    });
}

module.exports.create_user = function(req, res){
    var isOptionelCreate = (req.query.username && req.query.password &&
     req.query.age && req.query.lastdayranked && req.query.weeklyranked && req.query.score &&
     req.query.lastOnline && req.query.online ) ? true : false;
    if(isOptionelCreate){
        let newUser = new User({
            username: req.query.username,
            password: req.query.password,
            age: parseInt(req.query.age),
            lastdayranked: parseInt(req.query.lastdayranked),
            weeklyranked: parseInt(req.query.weeklyranked),
            score: parseInt(req.query.score),
            lastOnline: moment(req.query.lastOnline),
            online: Boolean(req.query.online) 
        });
        User.createUser(newUser, (err, data) => res.json({err: err, data: data, success: true }));
    }else{
        res.json({message: "kayit basarisiz", success: false });
    }
}

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? 1 : ((x > y) ? 0 : -1));
    });
}
function callUsers (iterator, rdata, ranks, res){
    if(iterator>rdata.length-1){
        ranks = sortByKey(ranks, "score");
        var i = 0;
        ranks.forEach(element => {
            ranks[i].rank = i+1;
            i++;
        });
        res.json({data: ranks})
    }
    else{
        User.findOne({_id: rdata[iterator].xid},function(err, data){
            ranks.push({
                username: data.username,
                score: data.score,
                age: data.age,
                rank: rdata[iterator].rank,
                lastdayranked: data.lastdayranked
            });
            iterator++;
            return callUsers(iterator, rdata, ranks, res);
        });
    }
}
module.exports.get_allRank = function (req, res){
    var ranks = [];
    Ranked.getAllUser(function(err, data){
        callUsers(0, data, ranks, res)
    });
}

module.exports.add_pointsecond = function (req, res){
    User.findOneAndUpdate(
        { username : req.query.id },
        { $inc: { score: 1 } },
        function(err, data) {
            res.json({success:true});
        }
    );
}