const User = require('../models/User');
const Ranked = require('../models/Ranked');
//const Coinpool = require('../models/Coinpool'); // or coinpool add and get, it's other algorithm
const moment = require("moment");

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? 1 : ((x > y) ? 0 : -1));
    });
}

module.exports.get_allRank = function(req, res){
    Ranked.getAllUser(function(err, data){
        res.json({
            data:data,
            err:err
        });
    })
}

module.exports.sort_Rank_All = function(req, res){
    var allow = false;
    if(req.query.hashbend){ // random 128 character 
        allow = (req.query.hashbend = "sBIfjo2SEYwELYhJMAv2XYoMal0FuHQPQ6UZmKTKyD9rFyFvmg5Jc0jreb5Y9MiEUgBUY4lujCTl5aUBHL7GV0DQQ8esS4uWB96WV5G1UGibnlefa28JLUMdXztBmudC")
                ?true:false;
    }
    if(allow){
        let today = moment().startOf('day');
        let AllOne =  User.find({ lastOnline: { $gte: today } })
        .sort({ $score: -1 })
        .limit(100)
        .exec(function(err,data){
            data = sortByKey(data, "score");
            Ranked.deleteMany({}).exec(function(){
                var i = 0;
                data.forEach(element => {i++;
                    let r = new Ranked({
                        username: element.username,
                        xid: element._id,
                        rank : i
                    });
                    Ranked.createUser(r);
                })
            });
            res.json({date:today, data:data});
            return;
        });
    }else{
        res.render('404');
        return;
    }
}

module.exports.day_ranked = function(req, res){
    var allow = false;
    if(req.query.hashbend){ // random 128 character 
        allow = (req.query.hashbend = "sBIfjo2SEYwELYhJMAv2XYoMal0FuHQPQ6UZmKTKyD9rFyFvmg5Jc0jreb5Y9MiEUgBUY4lujCTl5aUBHL7GV0DQQ8esS4uWB96WV5G1UGibnlefa28JLUMdXztBmudC")
                ?true:false;
    }
    if(allow){
        let today = moment().startOf('day');
        let AllOne =  User.find({ lastOnline: { $gte: today } })
        .sort({ $score: -1 })
        .limit(100)
        .exec(function(err,data){
            data = sortByKey(data, "score");
            data.forEach(element => {i++;
                let r = new Ranked({
                    username: element.username,
                    xid: element._id,
                    rank : i,
                    lastdayranked: i
                });
                Ranked.createUser(r);
            })
            res.json({date:today, data:data});
            return;
        });
    }else{
        res.render('404');
        return;
    }
}

module.exports.week_ranked = function(req, res){
    var allow = false;
    if(req.query.hashbend){ // random 128 character 
        allow = (req.query.hashbend = "sBIfjo2SEYwELYhJMAv2XYoMal0FuHQPQ6UZmKTKyD9rFyFvmg5Jc0jreb5Y9MiEUgBUY4lujCTl5aUBHL7GV0DQQ8esS4uWB96WV5G1UGibnlefa28JLUMdXztBmudC")
                ?true:false;
    }
    if(allow){
        let thisweek = moment().startOf('week');
        let AllOne =  User.find({ lastOnline: { $gte: thisweek } })
        .sort({ $score: -1 })
        .exec(function(err,data){
            var pool = 0;
            data = sortByKey(data, "score");
            data.forEach(element => {
                pool += (element.score/50);
            });
            var percent20 = (pool - (pool/5));
            var percent15 = (pool - ((pool*15)/100));
            var percent10 = (pool - (pool/10));
            var other = pool - percent10 - percent15 - percent20;
            other = other / 97;
            User.findOneAndUpdate(
                { _id: data[0]._id },
                {  $inc: { score: percent20 } } ,
                function(err, data) { }
            );
            User.findOneAndUpdate(
                { _id: data[1]._id },
                {  $inc: { score: percent20 } } ,
                function(err, data) { }
            );
            User.findOneAndUpdate(
                { _id: data[2]._id },
                {  $inc: { score: percent20 } } ,
                function(err, data) { }
            );
            for(var i=2; i<100; i++) {
                User.findOneAndUpdate(
                    { _id: data[i]._id },
                    {  $inc: { score: percent20 } } ,
                    function(err, data) { }
                );
            }
            res.json({date:today, data:data});
            return;
        });
    }else{
        res.render('404');
        return;
    }
}