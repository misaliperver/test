const path = require('path');
const express = require('express');
const ejsLayouts = require('express-ejs-layouts');
const app = express()
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
 const mongodb = require('mongodb');
 const User = require('./app_server/models/User');
 const mongoose = require('mongoose');
 const uri = "WHATHAPPEN?";
  mongoose.connect(uri, { useNewUrlParser: true });
  var db = mongoose.connection;

app
  .use(express.static(path.join(__dirname, 'public')))
  .set('view engine', 'ejs')
  .set('views', path.join(__dirname, './app_server/views'))
  .use(bodyParser.urlencoded({ extended: false }))
  .use(bodyParser.json())
  .use(cookieParser())
  .use(ejsLayouts)

require(path.join(__dirname, './app_server/routers/ManagerRouter.js'))(app);

const PORT = process.env.PORT || 8080;
const HOST = "0.0.0.0";
app.listen(PORT, HOST, function(){
  console.log(PORT + '. porttan server baslatildi.');
});