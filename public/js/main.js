angular.module("MyApp", ["ngCookies"])

.controller("tabCtrl", function($scope, $interval, $http, $cookieStore, $httpParamSerializer) {
    $scope.fiveofranks = [];
    $scope.ranks = [];
    var firstfetch = false;
    getallrank();//first fetch
    setInterval(() => {getallrank();}, 10000);
    function getallrank(){
        $http.get('/getAllRank')
        .success(function(response){
            if(!firstfetch){
                $('#scorerTable tbody').css('opacity', 1);
                $('.btn').css('opacity', 1);
                setTimeout(()=>{$('#nextbtn').click();}, 300);
            }
            firstfetch = true;
            $scope.ranks = response.data;
        });
    }
    var ranktop = 0;
    $scope.ranknext = function(){
        $scope.fiveofranks = [];
        if(ranktop <= $scope.ranks.length-1){
            for(var i=ranktop; i<ranktop+5; i++){
                if(i >= $scope.ranks.length)break;
                $scope.fiveofranks.push($scope.ranks[i]);
            }
            ranktop = i;
        }else{
            ranktop = 0;
            for(var i=ranktop; i<ranktop+5; i++){
                if(i >= $scope.ranks.length)break;
                $scope.fiveofranks.push($scope.ranks[i]);
            }
            ranktop = i;
        }
    }
    $scope.rankback = function(){
        ranktop=0;
        $scope.ranknext();
    }

    $scope.username = "fatih";
    update();
    function update(){
        $http.get('/addpointsecond?id='+$scope.username)
        .success(function(response){
            setTimeout(() => {update();}, 800);
        });
    }

});